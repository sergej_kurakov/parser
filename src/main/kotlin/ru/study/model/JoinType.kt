package ru.study.model

enum class JoinType {
    INNER,
    LEFT,
    RIGHT,
    FULL
}