package ru.study.model

data class Ordering(val column: String, val descending: Boolean = false)